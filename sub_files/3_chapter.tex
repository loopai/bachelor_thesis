\chapter{The \texttt{quicknn} package}
\label{chap:chap4}

A Python \texttt{package} is a set of \textit{modules}, i.e.\ source files written in Python language.
The package implemented in this work, called \texttt{quicknn}, consists in the core module \texttt{quicknn.py}, where a single large \texttt{class} is defined, and an auxiliary module \texttt{qnn\_config.py}, where some useful constants are defined.
\begin{lstlisting}[style=tree]
quicknn
├── qnn_config.py
└── quicknn.py
\end{lstlisting}

The class defined in \texttt{quicknn.py} is called \texttt{QuickNN} and can be entirely controlled through three main
%\footnote{In Python, the distinction between public, private and protected is only a convention and it is not so strictly as, for example, in C++ or Java.}
methods: \texttt{\_\_init\_\_}, \texttt{fit} and \texttt{predict}.
The parameters provided to these methods control the operations that the class can perform.

The \texttt{\_\_init\_\_} method is used to customize the class. More specifically, when a Python class is called, internally the \texttt{\_\_new\_\_} method creates an instance of the class, while the \texttt{\_\_init\_\_} method customizes it according to the parameters provided.

The \texttt{fit} method takes the data in input, the matrix of independent variables as well as the vector of dependent variable, checks the type (categorical or not) of each independent variable, builds the graph representing the structure of the network and, finally, optimizes the network parameters through \textit{training optimization algorithm}.

The \texttt{predict} method takes in input a matrix of independent variables and computes the corresponding vector of predictions.

\section{Prerequisites}

In order to correctly use the class, it is recommended the software \texttt{IPython} [\cite{PER-GRA:2007}] because it provides an interactive shell based on a Read-Eval-Print-Loop\footnote{\url{https://ipython.org/ipython-doc/3/development/how_ipython_works.html}} model which allows executions of different pieces of code separately in the time but in the same process, which means that different execution of code shared the same \textit{namespace}, and therefore the objects created in different executions, are available to future executions. The package \texttt{quicknn} depends on others packages which must be installed on the system:

\begin{itemize}
	\item [$\bullet$] \texttt{tensorflow} provides efficient computation of the gradient and many other functions used by neural networks;
	\item [$\bullet$] \texttt{numpy}\footnote{\url{http://www.numpy.org/}} provides generic math functions;
	\item [$\bullet$] \texttt{pandas}\footnote{\url{https://pandas.pydata.org/}} manages the data;
	\item [$\bullet$] \texttt{sklearn}\footnote{\url{http://scikit-learn.org/stable/}} provides (among others) the class \texttt{OneHotEncoder} to apply the \textit{one-hot encoding} method [\cite{geron2017hands}, pp. 62-64] to categorical variables, and the function \texttt{train\_test\_split} to split the dataset into training and test sets.
\end{itemize}

\section{How to install the package}
The \texttt{quicknn} package can be downloaded from the Python Package Index (PyPI)\footnote{\url{https://gitlab.com/deeplego/quicknn}} with the command:
\begin{minted}{bash}
$ pip install quicknn
\end{minted}
In addition, there is a repository on \url{https://gitlab.com/deeplego/quicknn} that contains the package and allow users to contribute to the project.

\section{How to use the package}
The use of the package is illustrated below through a couple of examples: the first explores the parameters of the different methods and shows the easiest way to use the package; more complex features are presented in the second example.

Each example is decomposed in pieces indexed by a natural number (n). For each piece it is reported the code (label \textcolor{mykey}{\footnotesize\ttfamily In$\,$[n]}), a description (label {\footnotesize\ttfamily [n]}) and the corresponding output (label \textcolor{mykey}{\footnotesize\ttfamily Out$\,$[n]})

%Each example has pieces of code indicated with \textcolor{mykey}{{\footnotesize\ttfamily In$\,$[n]}} and each is associated eventually with a description and an output indicated with {\footnotesize\ttfamily [n]} and \textcolor{mykey}{{\footnotesize\ttfamily Out$\,$[n]}} respectively, where {\footnotesize\ttfamily n} is the number that uniquely associates them.

Each piece of code is executed in a Linux system, but the same operations can be repeated on recent Windows and Mac systems. A new \texttt{IPython} session is assumed to be opened at the beginning of each example.
In both examples it is used the test set of the UCI ML hand-written digits datasets [\cite{Dua:2017}]. An hand-written digit on a software is decomposed in 8 $\times$ 8 = 64 pixels. For each of the 1797 observations, the dataset includes the true digit (0 - 9) as dependent variable and the 64 black intensities\footnote{The intensity is represented with a real number from 0 to 16.} of each pixel as independent variables (all continuous).

\subsection{First example: Description of the parameters}
%The dataset that will be used in both examples is a copy of the test set of the UCI ML hand-written digits datasets [\cite{Dua:2017}] and it has only continuous variables. 
%The dataset contains 1797 hand-written digits in black and white expressed in $8\times8$ pixels. In other terms it has 1797 observations and 64 features(or independent variables). Each series of these 64 pixels are associated to a label that indicates which digit from 0 to 9 is the true value of the image.
\begin{pyin}[py1]
from quicknn import QuickNN
\end{pyin}
\begin{description}
	\item[\ref{py1}] The \texttt{QuickNN} class is imported from the \texttt{quicknn.py} module.
\end{description}

\begin{pyin}[py2]
from sklearn.datasets import load_digits
X, y = load_digits(return_X_y=True)
\end{pyin}
\begin{description}
	\item[\ref{py2}] The dataset is imported from \texttt{sklearn.datasets} module, then the independent variables and the labels are stored in \texttt{X} and \texttt{y} respectively.
\end{description}

\begin{pyin}[py3]
qnn = QuickNN(list_neurons=[100, 200, 10],
	      problem='classification')
\end{pyin}
\begin{description}
	\item[\ref{py3}] An object of the \texttt{QuickNN} class is instantiated and it is customizable with the parameters of the \texttt{\_\_init\_\_} method, namely:
	\begin{description}[align=left]
		\item [\texttt{model\_name}] (\textit{string})\hfill \\ Name of the folder in which the model is saved. The default is \texttt{"qnn"}.
		\item [\texttt{list\_neurons}] (\textit{list of integers})\hfill \\ Each \textit{integer} except for the last one, represents the size of the hidden layer in the corresponding position. The last one represents the size of the output layer.
		\item [\texttt{save\_dir}] (\textit{string})\hfill \\ Directory, where model checkpoints are saved. Default path\footnote{This path does not depend on the operating system.}\\ \texttt{$\sim$/.local/share/quicknn/}.
		\item [\texttt{activation}] (\textit{function})\hfill \\ Activation function for each hidden layer. The default is the $\relu$ function.
		\item [\texttt{initializer}] (\textit{function})\hfill \\ Function indicating the way in which each parameter of the network is initialized. The default is the truncated normal initializer: it takes values from a normal distribution with mean 0 and standard deviation 0.5; the values far from the mean (for more than two standard deviations) are discarded and re-drawn. Any tensorflow initializer from \url{https://www.tensorflow.org/api_docs/python/tf/keras/initializers} can be chosen.
		\item [\texttt{training\_func}] (\textit{function})\hfill \\ Optimization algorithm to minimize the cost function. The default is the gradient descent. Any tensorflow optimizer from \url{https://www.tensorflow.org/api_docs/python/tf/keras/optimizers} can be chosen.
		\item [\texttt{problem}] (\textit{string})\hfill \\ \texttt{"regression"} (the default) or \texttt{"classification"}.
	\end{description}
\end{description}

\begin{pyin}[py4]
qnn.fit(X, y, validation=True)
\end{pyin}
\begin{pyout}
Epoch: 1 | train_accuracy: 0.0633 | val_accuracy: 0.0384
Epoch: 2 | train_accuracy: 0.0608 | val_accuracy: 0.0340
Epoch: 3 | train_accuracy: 0.0584 | val_accuracy: 0.0300
Epoch: 4 | train_accuracy: 0.0658 | val_accuracy: 0.0392
Epoch: 5 | train_accuracy: 0.0781 | val_accuracy: 0.0551
\end{pyout}
\begin{description}
	\item[\ref{py4}] The \texttt{fit} method builds the graph according to the structure in \texttt{list\_neurons}, then it starts the model training executing a number of iterations equal to $\frac{n}{\texttt{batch\_size}} * \texttt{n\_epochs}$, where $n$ is the total of the observations in the training set. While performing the training phase, a score is computed at each iteration and a mean of these scores\footnote{The mean of the scores is computed epoch-by-epoch.} is printed at the end of each epoch.
	If the data provided have categorical variables (according to the \textit{one-hot encoding} method), each of these are replaced with an amount of dummy variables equal to the total number of unique values that the category variable can assume. To handle categorical variables, the matrix of the independent variables must be fed with a \texttt{pandas.DataFrame} object, in which the \texttt{dtypes} of the categorical columns are all '\textit{object}' [\cite{pandas_cookbook}, p. 62]. It is not an heavy burden to manage internally categorical data, since \texttt{pandas} has a lot of IO tools to read data formats\footnote{The complete list of all \texttt{pandas} IO tools is available at \url{https://pandas.pydata.org/pandas-docs/stable/io.html}}. The parameters of the \texttt{fit} method are:
	\begin{description}[align=left]
		\item [\texttt{X}] (\textit{array})\hfill \\ Matrix of values of the ndependent variables in the training set. If there are categorical variables, \texttt{X} must be a \texttt{pandas.DataFrame} object, as explained above.
		\item [\texttt{y}] (\textit{array})\hfill \\ Vector of the dependent variable in the training set.
		\item [\texttt{batch\_size}] (\textit{integer})\hfill \\ The size of the batch, that is the portion of data feeding the network at each iteration. The default is 16 [\cite{keskar2016large}].
		\item [\texttt{n\_epochs}] (\textit{integer})\hfill \\ Number of epochs, namely how many times the model visits the whole training set in average. At every epoch, the training set is shuffled and split in $\left\lfloor \frac{n} {\texttt{batch\_size}}\right\rfloor $ number of batches. Then the model is fed with one batch per iteration. When batches of an epoch are terminated, a new epoch restarts reshuffling the data and dividing it in batches and so on until \texttt{n\_epochs} is reached. The default value is 5.
		\item [\texttt{learning\_rate}] (\textit{float})\hfill \\ The $\eta$ hyperparameter in Eq. [\ref{eq:eq210}]: is a scalar between 0 and 1. The default value is set to 0.001.
		\item [\texttt{list\_dropout\_rate}] (\textit{list of floats})\hfill \\ Each between 0 and 1, with the same size as \texttt{list\_neurons}. It indicates whether or not to use the \textit{dropout} technique [\cite{srivastava2014dropout}] in each layer except for the output one. For example, in order to apply this technique with a dropout rate of 0.5 to both the input and the third layer of a network made by 3 hidden layers with \texttt{list\_neurons} set to [100, 200, 200, 1], \texttt{list\_dropout\_rate} must be [0.5, 0, 0, 0.5].
		\item [\texttt{validation}] (\textit{boolean})\hfill \\ Allows validation of the model on a part of the training set (refer to \texttt{test\_size}) randomly chosen after each iteration. Default value set to \texttt{False}.
		\item [\texttt{test\_size}] (\textit{float})\hfill \\ A value between 0 and 1. It indicates the fraction of the training set used to validate the model at each iteration of the training phase.
		\item [\texttt{func\_y}] (\textit{function})\hfill \\ Maps the target variable (example $\log y$ instead of $y$), if set it requires the inverse function to validate the results.
		\item [\texttt{inv\_func\_y}] (\textit{function})\hfill \\ Inverse function of \texttt{func\_y}. For example, if \texttt{func\_y} is $\log$ than \texttt{inv\_func\_y} must be $\exp$.
		\item [\texttt{restore\_prev}] (\textit{boolean})\hfill \\ If \texttt{True} the model parameters of the previous checkpoint are restored. At every epoch the parameters of the model are saved into the \\\texttt{"<save\_dir><path separator\footnote{It depends on operating system.}><model\_name>"} folder.
		\item [\texttt{metric}] (\textit{function})\hfill \\ It is used to change the default \textit{metric} for the given problem.\\ In \texttt{'classification'},\ the default {metric} is the accuracy\footnote{The accuracy is the fraction correctly predicted values:$$accuracy=\frac{true\ positive + true\ negative}{Total observations}$$ }, for \\ \texttt{'regression'} the default is the $\mse$.
	\end{description}
\end{description}

\begin{pyin}[py5]
y_probs = qnn.predict(X)
\end{pyin}
\begin{pyout}
INFO:tensorflow:Restoring parameters from
/home/lore/.local/share/quicknn/tf_saver/qnn/qnn-81
\end{pyout}
\begin{description}
	\item[\ref{py5}] The \texttt{predict} method loads the last parameters saved and makes predictions, executing a forward pass on data supplied using the graph built previously by \texttt{fit}. When the \texttt{problem} parameter is set to \texttt{'classification'}, the output of this method is a matrix of the logit values of size $n \times J$, where $n$ is the number of observations and $J$ the number of classes. The only parameter of \texttt{predict} uses is
		\begin{description}[align=left]
		\item [\texttt{X}] (\texttt{X})\hfill \\ Independent variables used to make predictions. It must have the same variables of the training set passed to \texttt{fit}.
		\end{description}
\end{description}


\subsection{Second example: How to stop-resume the training}
In the previous example, only the essential parameters involved in the training phase were modified, giving little importance to the efficiency in the prediction phase. This example shows how the parameters can be modified during model training to obtain more accurate predictions.

The crucial point is that the training phase can be interrupted, the parameters modified and the model training resumed with the new settings from the last checkpoint produced before the interruption. The way in which the training can be stopped depend on the software utilized and the operating system. In a Linux system running the \texttt{IPython} session through a terminal, the stop is given by the combination keys \texttt{Ctrl-C}.
\\
\begin{pyin}[py6]
from sklearn.datasets import load_digits
X, y = load_digits(return_X_y=True)
\end{pyin}

\begin{pyin}[py7]
from sklearn.preprocessing import StandardScaler
sc = StandardScaler()
X_sc = sc.fit_transform(X)
\end{pyin}

\begin{pyin}[py8]
from sklearn.model_selection import train_test_split
X_train, X_val, y_train, y_val = \
train_test_split(X_sc, y, test_size=0.20)
\end{pyin}

\begin{pyin}[py9]
from quicknn import QuickNN
qnn = QuickNN(list_neurons=[100, 200, 10],
              problem='classification')
\end{pyin}

\begin{pyin}[py10]
qnn.fit(X_train, y_train, validation=True)
\end{pyin}
\begin{pyout}
Epoch: 1 | train_accuracy: 0.0641 | val_accuracy: 0.0576
Epoch: 2 | train_accuracy: 0.0683 | val_accuracy: 0.0875
Epoch: 3 | train_accuracy: 0.0814 | val_accuracy: 0.1134
Epoch: 4 | train_accuracy: 0.1011 | val_accuracy: 0.1174
Epoch: 5 | train_accuracy: 0.1094 | val_accuracy: 0.1277
\end{pyout}

Until \textcolor{mykey}{{\scriptsize In [10]}}, this example is almost the same than the previous one except for a couple of elements:
\begin{itemize}
	\item[\ref{py7}] The method \texttt{fit\_transform} of the class \texttt{StandardScaler} is imported to allow of the variables standardization\footnote{The method executes $\frac{z\ -\  mean(z)}{standard\_deviation(z)}\ \forall z$, to each column of the input \texttt{X}.}. Additionally, it saves the mean and the standard deviation of each column for future inverse transformation.

	The matrix of independent variables standardized are stored in the \texttt{X\_sc} variable.
	
	\item[\ref{py8}] The \texttt{train\_test\_split} function split data by row using the ratio provided ($0.20$ in this case). In other terms, it takes the training set and split it horizontally into a new training set and a validation set with respectively the 80\% and the 20\% of the observations of the original training set. The last one is called 'validation' because it is used to test how the predictions of a generic model are generalizable, since the model will not see this data during the training phase. The model then is trained on the training set and tested on the validation set, and the hyperparameters are adjusted in order to achieve more accurate predictions on the validation set. When the accuracy is satisfying, the model is trained on the whole dataset (with \texttt{validation} set to \texttt{True}) predictions is made supplying new observations of independent variables (test set).

\end{itemize}
The results in \textcolor{mykey}{{\scriptsize Out [10]}} are not satisfying because the validation accuracy grows slowly (less than 10\% in 5 epochs). To achieve better accuracy, in \ref{py11} the \texttt{learning\_rate} is increased by an order of magnitude setting the \texttt{restore\_prev} parameter to \texttt{True}. The package retrieves the parameters of the 380-th iteration, which is be obtained as $$\frac{1797 * (1 - 0.2) \times (1 - 0.15)}{16} \times 5 = 380$$
\begin{itemize}
	\item 1797 = number observations in the whole dataset;
	\item 0.2 = \texttt{train\_test\_split} function;
	\item 0.15 = \texttt{test\_size} of the \texttt{fit} method;
	\item 16 = \texttt{batch\_size};
	\item 5 = \texttt{n\_epochs}.
\end{itemize} With the \texttt{n\_epochs} augmented to 10, the algorithm considers 5 more epochs.

\begin{pyin}[py11]
qnn.fit(X_train, y_train, n_epochs=10, batch_size=16,
        learning_rate=0.01, validation=True,
        restore_prev=True)
\end{pyin}
\begin{pyout}
INFO:tensorflow:Restoring parameters from
home/lore/.local/share/quicknn/tf_saver/qnn/qnn-380
Epoch: 6 | train_accuracy: 0.1973 | val_accuracy: 0.2132
Epoch: 7 | train_accuracy: 0.3462 | val_accuracy: 0.3338
Epoch: 8 | train_accuracy: 0.4550 | val_accuracy: 0.4312
Epoch: 9 | train_accuracy: 0.5273 | val_accuracy: 0.5382
Epoch: 10 | train_accuracy: 0.5926 | val_accuracy: 0.6048
\end{pyout}
As shown in \textcolor{mykey}{{\scriptsize Out [11]}}, once increased the \texttt{learning\_rate}, there is already a growth to 10\% of validation accuracy from 5-th to 6-th epoch. In the 10-th epoch the same index reaches 60\%. Therefore, in \textcolor{mykey}{{\scriptsize In [12]}} the \texttt{n\_epochs} is increased to 20 (all other parameter left unchanged) and the algorithm performs others 10 epochs that carry the accuracy to 86\%.
\begin{pyin}[py12]
qnn.fit(X_train, y_train, n_epochs=20, batch_size=16,
        learning_rate=0.01, validation=True,
        restore_prev=True)
\end{pyin}
\begin{pyout}
INFO:tensorflow:Restoring parameters from
home/lore/.local/share/quicknn/tf_saver/qnn/qnn-760
Epoch: 11 | train_accuracy: 0.6622 | val_accuracy: 0.6840
Epoch: 12 | train_accuracy: 0.7085 | val_accuracy: 0.7269
Epoch: 13 | train_accuracy: 0.7345 | val_accuracy: 0.7595
Epoch: 14 | train_accuracy: 0.7507 | val_accuracy: 0.7823
Epoch: 15 | train_accuracy: 0.7752 | val_accuracy: 0.7969
Epoch: 16 | train_accuracy: 0.7942 | val_accuracy: 0.8084
Epoch: 17 | train_accuracy: 0.8075 | val_accuracy: 0.8145
Epoch: 18 | train_accuracy: 0.8202 | val_accuracy: 0.8277
Epoch: 19 | train_accuracy: 0.8307 | val_accuracy: 0.8425
Epoch: 20 | train_accuracy: 0.8490 | val_accuracy: 0.8605
\end{pyout}
The results achieved in \textcolor{mykey}{{\scriptsize Out [12]}} are satisfying so that in \textcolor{mykey}{{\scriptsize In [13]}} the model is trained on the whole training set for additional 10 epochs reaching almost 92\% of training accuracy.
\begin{pyin}[py13]
qnn.fit(X_train, y_train, n_epochs=30, batch_size=16,
        learning_rate=0.01,
        restore_prev=True)
\end{pyin}
\begin{pyout}
INFO:tensorflow:Restoring parameters from
/home/lore/.local/share/quicknn/tf_saver/qnn/qnn-1520
Epoch: 21 | accuracy: 0.8609
Epoch: 22 | accuracy: 0.8735
Epoch: 23 | accuracy: 0.8771
Epoch: 24 | accuracy: 0.8883
Epoch: 25 | accuracy: 0.8953
Epoch: 26 | accuracy: 0.8995
Epoch: 27 | accuracy: 0.9037
Epoch: 28 | accuracy: 0.9080
Epoch: 29 | accuracy: 0.9171
Epoch: 30 | accuracy: 0.9234
\end{pyout}

\begin{pyin}[py14]
y_probs = qnn.predict(X_val)
\end{pyin}
\begin{pyout}
INFO:tensorflow:Restoring parameters from
/home/lore/.local/share/quicknn/tf_saver/qnn/qnn-2280
\end{pyout}
\begin{description}
	\item[\ref{py14}] Since the model reached a good level of training, the \texttt{predict} is called to compute the predicted probabilities. The output is stored in the {\footnotesize y\_probs} variable.
\end{description}
\begin{pyin}[py15]
import numpy as np
def top_class(y_pred, labels=None):
    if labels is None:
        labels=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    labels_pred = []
    for i in range(y_pred.shape[0]):
        topp = y_pred[i, 0]
        topk = labels[0]
        for j in range(y_pred.shape[1]):
            if y_pred[i, j] > topp:
                topp = y_pred[i, j]
                topk = labels[j]
        labels_pred.append(topk)
    return np.array(labels_pred)
\end{pyin}

\begin{pyin}[py16]
y_pred = top_class(y_probs)
\end{pyin}

%\begin{description}
%	\item[\ref{py15}] The \texttt{top\_class} function is implemented to transform each row of the logits matrix in a label, which corresponds with the high logit in the relative row. The \texttt{predict} does not return directly the labels because not always the classes are balanced. In the \texttt{top\_class} function there is the assumption that each class has the same weight, i.e. the frequency of the classes are almost equal as in the digits dataset(Fig. [\ref{fig:fig31}]). When the class are unbalanced, for example as in Fig. [], then the logits must be balanced in order to have reasonable results.
%	\begin{figure}[H]
%%		\centering
%%		\includegraphics[width=1.1\textwidth, height=0.3\textheight]{../images/sigmoid_function.png}
%		\includegraphics[width=1.1\textwidth]{../images/digits_barplot.png}
%		\caption{The frequencies in the hand-written digits dataset are almost equal for each class.}
%		\label{fig:fig31}
%		\small
%		\centering
%	\end{figure}
%\end{description}

\begin{description}
	\item[\ref{py15}] The \texttt{top\_class} function transforms each row of the \texttt{y\_probs matrix} in the label, corresponding to the highest probability.
	\item[\ref{py16}] The array of predictions is stored in the \texttt{y\_pred} variable.
\end{description}

\begin{pyin}[py18]
import matplotlib.pyplot as plt
img = np.reshape(sc.inverse_transform(X_val[123]),(8, 8))
plt.imshow(img, cmap="binary", interpolation="nearest")
plt.xticks(np.arange(0, 8), np.arange(1, 9))
plt.yticks(np.arange(0, 8), np.arange(1, 9))
plt.show()
\end{pyin}
\begin{pyout}
\
\end{pyout}
%\hspace*{2cm}\includegraphics[width=7cm]{../images/4_digit.png}
	\begin{figure}[H]
		\includegraphics[width=0.5\textwidth]{../images/4_digit.png}
		\small
		\centering
	\end{figure}
\begin{pyin}[py19]
y_pred[123]
\end{pyin}
\begin{pyout}
4
\end{pyout}

\begin{pyin}[py20]
y_val[123]
\end{pyin}
\begin{pyout}
4
\end{pyout}

\begin{description}
	\item[\ref{py17}] Using the \texttt{matplotlib}\footnote{https://matplotlib.org/} library, the 123-th observation from the validation set is displayed. This is compared with the prediction obtained \ref{py18} and with the corresponding true label \ref{py19}. Obviously, the fact that the neural network predicts correctly this observation does not mean that it also able to predict well the others.
\end{description}

\begin{pyin}[py17]
def accuracy(y_true, y_pred):
return np.mean(y_true == y_pred)
accuracy(y_val, y_pred)
\end{pyin}
\begin{pyout}
	0.8444
\end{pyout}

\begin{description}
	\item[\ref{py17}] To measure how much the model is accurate in predictions, the accuracy function is defined. Then it is applied to the true and the predicted labels returning about 84\% of correct predictions.
\end{description}

%\subsection{Third example - Categorical variables}
%The \texttt{quicknn} package can handle categorical variables, but only if the data is given as \texttt{pandas object}. On the other hand, \texttt{pandas} has a lot of IO tools which allows the reading of many different formats\footnote{A complete list of all IO tools of \texttt{pandas} is available on https://pandas.pydata.org/pandas-docs/stable/io.html}.
%This time the example presented comprises a regression problem, categorical variables and an application of the dropout technique.
%\begin{pyin}[py21]
%import pandas as pd
%X = pd.read_csv(X_filename)
%y = pd.read_csv(y_filename)
%X_test = pd.read_csv(X_test_filename)
%\end{pyin}
%\begin{description}
%	\item[\ref{py21}] The {\footnotesize X\_filenames}, {\footnotesize X\_test\_filename} and {\footnotesize y\_filename} variables store the system paths to relative data(the name of the variables is self-explanatory) in CSV format. 
%\end{description}
%\begin{pyin}
%from quicknn import QuickNN
%qnn = QuickNN(list_neurons=[100, 200, 300, 1],
%              list_dropout=[0.1, 0.1, 0.1, 0.1],
%              problem='regression', test_size=0.3)
%qnn.fit(X, y, validation=True)
%y_pred = qnn.predict(X_test)
%\end{pyin}

\subsection{Visualizing learning curves using Tensorboard}
When the \texttt{fit} method is launched, at every epoch the package reports summary information about the learning of the model. To show these this summary at every iteration, instead that a every epoch, there is a suite of visualization tools called \texttt{Tensorboard} that can be launched in Linux systems using the shell command:
\begin{minted}{bash}
$ tensorboard --logdir ~/.local/share/quicknn/tf_logs/*
\end{minted}
Once \texttt{Tensorboard} is running, it will be accessible by any browser \url{http://localhost:6006/}\footnote{The port 6006 is 'goog' written upside down.}.
\texttt{Tensorboard} is a powerful tool that provides, among other, options to visualize the learning curve of a model while training, which is helpful to tune the hyperparameters. For example, to understand which value of \texttt{learning\_rate} to choose, you may try a low value (e.g. $0.0001$): the result after 30 epochs and \texttt{validation} set to \texttt{True} is depicted in Fig. [\ref{fig:fig31}] where, in both graphs, the horizontal axes represent the number of iterations and the vertical ones, the accuracy evaluated on training set (top graph) and on validation set (bottom graph). After that, you might try another value of \texttt{learning\_rate} to see how the trend of the learning curves changes. To achieve this, you have only to reset the current \texttt{IPython} session and relaunch the training with different \texttt{learning\_rate}, for example $0.0005$. \texttt{Tensorboard} will update itself as in Fig. [\ref{fig:fig32}]. This procedure can be repeated, e.g. with the values $0.001$, $0.005$ and $0.01$. At the end the result looks like in Fig. [\ref{fig:fig33}]. The figure can be used to guess the best value of the \texttt{learning\_rate}, in this case $0.005$ because associated to the highest curve.
\begin{figure}[H]
	\centering
	\includegraphics[width=1\textwidth, height=0.35\textheight]{../images/tensorboard_1.png}
	\caption{Model learning (\texttt{learning\_rate} set to $0.0001$).}
	\label{fig:fig31}
	\small
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width=1\textwidth, height=0.35\textheight]{../images/tensorboard_2.png}
	\caption{Model learning comparison (\texttt{learning\_rate} \textcolor{orange}{orange}: $0.0001$; \textcolor{blue}{blue}: $0.0005$).}
	\label{fig:fig32}
	\small
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width=1\textwidth, height=0.35\textheight]{../images/tensorboard_5.png}
	\caption{Model learning comparison (\texttt{learning\_rate} \textcolor{orange}{orange}: $0.0001$; \textcolor{blue}{blue}: $0.0005$; \textcolor{brown}{brown}: $0.001$; \textcolor{cyan}{light blue}: $0.005$; \textcolor{pink}{Pink}: $0.01$).}
	\label{fig:fig33}
	\small
\end{figure}

\texttt{Tensorboard} allows also the visualization of the computational graph that represents the model trained, just clicking in the upper-left tab "GRAPHS" the result will look like as in Fig. [\ref{fig:fig34}].
\begin{figure}[H]
	\centering
	\includegraphics[width=1\textwidth, height=0.35\textheight]{../images/graph.png}
	\caption{Computational graph displayed using \texttt{Tensorboard}.}
	\label{fig:fig34}
	\small
\end{figure}